import java.util.*;


public class MapSort {
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {

        class PersonComparator implements Comparator<Integer> {
            public int compare(Integer a, Integer b){
                return b.compareTo(a);
            }
        }
        PersonComparator comparator = new PersonComparator();
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());

        list.sort(Map.Entry.comparingByValue((Comparator<? super V>) comparator));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}