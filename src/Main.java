import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] argv) throws IOException {
        Reader reader = null;
        Integer numbWords = 0;
        Map<String, Integer> stringIsKey = new HashMap<String, Integer>();
        Scanner scanner = new Scanner(System.in);
        FileWriter writer = new FileWriter("out.csv");
        System.out.println("Enter file name:\n");
        String fileName = scanner.next();
            reader = new InputStreamReader(new FileInputStream(fileName));
            char[] tmpStr = new char[50];
            StringBuilder word = new StringBuilder();
            while(reader.ready()) {
                reader.read(tmpStr, 0, 50);
                // parse piece of string
                for(int i = 0; i < tmpStr.length; ++i) {
                    if(Character.isLetterOrDigit(tmpStr[i])) {
                        word.append(tmpStr[i]);
                    } else if(word.length() != 0) {
                        stringIsKey.put(word.toString(), (stringIsKey.get(word.toString()) == null) ? 1 : stringIsKey.get(word.toString()) + 1);
                        word.delete(0, word.length());
                        numbWords++;
                    }
                }
            }
            stringIsKey = MapSort.sortByValue(stringIsKey);
            for(Map.Entry<String, Integer> iter: stringIsKey.entrySet()) {
                String tmp = String.format("%.2f", ((double)iter.getValue() / (double)numbWords)*100);
                writer.write(iter.getKey() + ";" + iter.getValue() + ';' + tmp + '%' + '\n');
            }
            writer.flush();
    }
}

